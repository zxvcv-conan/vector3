Changelog
=========

0.1.1 (2022-11-08)
------------------
- Template functions for 3D vector operations.

0.1.0 (2022-11-08)
------------------
- Initial.
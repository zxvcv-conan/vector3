// ====================================================================================
//                                LICENSE INFORMATIONS
// ====================================================================================
// Copyright (c) 2020-2022 Pawel Piskorz
// Licensed under the Eclipse Public License 2.0
// See attached LICENSE file
// ====================================================================================

#ifndef ZXVCV_VECTOR3_MATH_H_
#define ZXVCV_VECTOR3_MATH_H_


// ================================== DATA TYPES ======================================

/** 3D point object type */
typedef
/** Definitions of 3D point object
 *
 * For store three dimention points as single value.
 */
struct point3D_Tag{
    /** X axis value */
    double x;

    /** Y axis value */
    double y;

    /** Z axis value */
    double z;
}point3D;

/** 3D vector object type */
typedef
/** Definitions of 3D vector object
 *
 * For store three dimention vector as single value.
 */
struct vect3D_Tag{
    /** X axis value */
    double x;

    /** Y axis value */
    double y;

    /** Z axis value */
    double z;
}vect3D;


// ============================== PUBLIC DECLARATIONS =================================

/** Create vector P1 P2.
 *
 * P1 is the begining of the vecor, and P2 is the end of it.
 * Function will return vector object based on passed values.
 *
 * @param p1 X axis value of point P1
 * @param p2 X axis value of point P2
 *
 * @return created vector
 */
vect3D vect3D_get(point3D p1, point3D p2);

/** Return length of the vector.
 *
 * @param v vector which lenght we wanna get
 *
 * @return v vector lenght
 */
double vect3D_length(vect3D v);

/** Return scalar product of two vectors
 *
 * @param v1 vector 1
 * @param v2 vector 2
 *
 * @return vectors scalar product
 */
double vect3D_scalar_product(vect3D v1, vect3D v2);

/** Return angle between two vectors.
 *
 * @param v1 vector 1
 * @param v2 vector 2
 *
 * @return angle in radians
 */
double vect3D_angle(vect3D v1, vect3D v2);

vect3D velocity_vect3D(vect3D move, double v);


#endif // ZXVCV_VECTOR3_MATH_H_
